﻿using System;
using NetVips;
using ImageProcessing;
using ImageProcessingCore;
using ImageProcessingUtils;
using System.Collections.Generic;

namespace PhotoRealistic
{
    class Program
    {
        public static string fname = @"D:\Work\PhotoRealistic\4Tankola.png";
        public static string savePath = @"D:\Texture\";
        public static string projectPath = AppDomain.CurrentDomain.BaseDirectory;

        static void Main(string[] args)
        {
            NetVips.NetVips.CacheSetMax(0);
            NetVips.Image img = NetVips.Image.NewFromFile(fname, access: Enums.Access.Random);
            //BumpMapFilter(img);
            //CarvingMapFilter(img);
            //var bumpMap = CreateBumpMap(fname, 1, img);
            //CreateCarvingMap(img, 5, 128);
            //NetVips.Image img = NetVips.Image.NewFromFile(fname, access: Enums.Access.Sequential);

            var prDetails = GeneratePhotoRealisticData(fname);
            Applytexture(fname, prDetails);


            //// Copy from Memory
            //int h = img.Height; int w = img.Width;
            //int bandNumber = img.Bands;
            //var i = img.WriteToMemory();
            //var copy_img = NetVips.Image.NewFromMemory(i, w, h, bandNumber, "uchar");
            //var rolled = copy_img.Wrap(2, 5);
            //rolled.WriteToFile(@"D:\WORK\rolled.jpg");
            //rolled = rolled.Wrap(100, 100);
            //rolled.WriteToFile(@"D:\WORK\rolled2.jpg");

        }

        public static List<Tuple<int[], int, int, int>> GeneratePhotoRealisticData(string fname)
        {
            Surface img = SurfaceUtils.SurfaceFromFile(fname);
            var colors = img.GetUsedColorInfo();
            List<Tuple<int[], int, int, int>> prColorDetails = new List<Tuple<int[], int, int, int>>();
            Random rnd = new Random();
            for (int i = 0; i < colors.Count; i++)
            {
                int textureFile = rnd.Next(0, 3);
                int pileHeight = rnd.Next(1, 5);
                int carvingInfo = rnd.Next(1, 3);
                prColorDetails.Add(Tuple.Create(new[] { (int)colors[i].DisplayColor.R, (int)colors[i].DisplayColor.G, (int)colors[i].DisplayColor.B }, textureFile, pileHeight, carvingInfo));
            }
            return prColorDetails;
        }

        public static NetVips.Image CreateBumpMap(string filename, int factor, NetVips.Image img)
        {
            img = img.Colourspace("b-w") / 255;
            int h = img.Height; int w = img.Width;
            int bandNumber = img.Bands;
            var format = img.Format;
            var i = img.WriteToMemory();

            //var imageOne = NetVips.Image.NewFromFile(filename, access: Enums.Access.Sequential).Colourspace("b-w")/255;
            //var imageTwo = NetVips.Image.NewFromFile(filename, access: Enums.Access.Sequential).Colourspace("b-w")/255;
            var blackImage = NetVips.Image.Black(w, h, 1);
            var whiteImage = blackImage + 255;
            var identityImage = blackImage + 1;

            //imageOne = imageOne / 255;
            //imageTwo = imageTwo / 255;
            //var one_image = (black_image == 0).ifthenelse(1, black_image);

            //var moveLeft = imageOne.Wrap(x: -factor, y: 0);
            //var moveRight = imageOne.Wrap(x: factor, y: 0);
            //var moveDown = imageTwo.Wrap(x: 0, y: -factor);
            //var moveUp = imageTwo.Wrap(x: 0, y: factor);

            var moveLeft = NetVips.Image.NewFromMemory(i, w, h, bandNumber, format).Wrap(x: -factor, y: 0);
            var moveRight = NetVips.Image.NewFromMemory(i, w, h, bandNumber, format).Wrap(x: factor, y: 0);
            var moveDown = NetVips.Image.NewFromMemory(i, w, h, bandNumber, format).Wrap(x: 0, y: -factor);
            var moveUp = NetVips.Image.NewFromMemory(i, w, h, bandNumber, format).Wrap(x: 0, y: factor);

            //var dx = moveRight.Subtract(moveLeft);
            //var dy = moveUp.Subtract(moveDown);

            var dx = moveLeft.Subtract(moveRight);
            var dy = moveDown.Subtract(moveUp);

            var denominator = (dx * dx + dy * dy + 1).Pow(0.5);

            var nx = dx.Divide(denominator);
            var ny = dy.Divide(denominator);
            var nz = (identityImage).Divide(denominator);

            var r_cal = ((nx + 1).Divide(identityImage + 1)).Multiply(whiteImage);
            var g_cal = ((ny + 1).Divide(identityImage + 1)).Multiply(whiteImage);
            var b_cal = nz.Multiply(whiteImage);

            var bumpMap = (r_cal * 0.2126 + g_cal * 0.7152 + b_cal * 0.0722).Cast("uchar");
            bumpMap.WriteToFile(savePath+"BumpedNormal.jpg");

            return bumpMap;
        }

        public static NetVips.Image CreateNormalMap(string filename, int factor)
        {
            var imageOne = NetVips.Image.NewFromFile(filename, access: "random") / 255;
            var imageTwo = NetVips.Image.NewFromFile(filename, access: "random") / 255;
            var blackImage = NetVips.Image.Black(imageOne.Width, imageOne.Height, 1);
            var whiteImage = blackImage + 255;
            var identityImage = blackImage + 1;


            var bandsImgOne = imageOne.Bandsplit();
            var bandsImgTwo = imageOne.Bandsplit();

            var summedImageOne = bandsImgOne[0] + bandsImgOne[1] + bandsImgOne[2];
            var summedImageTwo = bandsImgTwo[0] + bandsImgTwo[1] + bandsImgTwo[2];

            imageOne = summedImageOne * 0.7;
            imageTwo = summedImageOne * 0.7;
            //var one_image = (black_image == 0).ifthenelse(1, black_image);


            var moveRight = imageOne.Wrap(x: factor, y: 0);
            var moveLeft = imageOne.Wrap(x: -factor, y: 0);
            var moveUp = imageTwo.Wrap(x: 0, y: factor);
            var moveDown = imageTwo.Wrap(x: 0, y: -factor);

            var dx = moveRight.Subtract(moveLeft);
            var dy = moveUp.Subtract(moveDown);

            var denominator = (dx * dx + dy * dy + 1).Pow(0.5);

            var nx = dx.Divide(denominator);
            var ny = dy.Divide(denominator);
            var nz = (identityImage).Divide(denominator);

            var r_cal = ((nx + 1).Divide(identityImage + 1)).Multiply(whiteImage);
            var g_cal = ((ny + 1).Divide(identityImage + 1)).Multiply(whiteImage);
            var b_cal = nz.Multiply(whiteImage);

            var normalMap = r_cal.Bandjoin(g_cal).Bandjoin(b_cal).Cast("uchar");
            normalMap.WriteToFile(savePath+"normal_map.jpg");


            return normalMap;
        }

        public static NetVips.Image CreateCarvingMap(NetVips.Image bumpMap, int low, int high)
        {
            var moveRight = bumpMap.Wrap(x: 5, y: 0);
            var moveUp = bumpMap.Wrap(x: 0, y: 5);
            var outline = (bumpMap - moveRight).Abs() + (bumpMap - moveUp).Abs();
            outline = outline.Ifthenelse(2, 255);
            var kernel = Image.NewFromArray(new[,]
                                            {
                                                {1/9, 1/9, 1/9},
                                                {1/9, 1/9, 1/9},
                                                {1/9, 1/9, 1/9}
                                            });
            //outline = outline.Conv(kernel);
            //outline = outline.Gaussblur(1);
            outline.WriteToFile(savePath+"OutlineMap.jpg");
            var carvingMap = CreateBumpMap(savePath+"OutlineMap.jpg", 1, outline);
            carvingMap.WriteToFile(savePath+"CarvingMap.jpg");
            return carvingMap;
        }

        public static NetVips.Image Applytexture(string filename, List<Tuple<int[], int, int, int>> prDetails)
        {
            string[] textureNames = new string[] { projectPath+"Textures\\custom.jpg", projectPath + "Textures\\loop.jpg", projectPath + "Textures\\silk.jpg", projectPath + "Textures\\wool.jpg" };
            NetVips.Image[] textureImages = new NetVips.Image[textureNames.Length];
            NetVips.Image originalImage = NetVips.Image.NewFromFile(filename, access: Enums.Access.Sequential);
            var targetColors = new[] { new[] { 200, 200, 200 }, new[] { 50, 50, 50 } };

            if (originalImage.HasAlpha()) { originalImage = originalImage.Flatten(); }

            for (int i = 0; i < textureNames.Length; i++)
            {
                textureImages[i] = NetVips.Image.NewFromFile(textureNames[i], access: Enums.Access.Sequential).Crop(0, 0, originalImage.Width, originalImage.Height);
            }

            //NetVips.Image textureAdded = NetVips.Image.Black(originalImage.Width, originalImage.Height);
            var textureAdded = originalImage;
            var bumpIndex = NetVips.Image.Black(originalImage.Width, originalImage.Height, 3)+255;
            var carvingIndex = bumpIndex;

            for (int i = 0; i < prDetails.Count; i++)
            {
                textureAdded = (originalImage.Equal(prDetails[i].Item1)).Ifthenelse(textureAdded + textureImages[prDetails[i].Item2] - 128 , textureAdded);
                bumpIndex = (originalImage.Equal(prDetails[i].Item1)).Ifthenelse(prDetails[i].Item3*51, bumpIndex);
                carvingIndex = (originalImage.Equal(prDetails[i].Item1)).Ifthenelse(prDetails[i].Item4*85, carvingIndex);
            }
            //carvingIndex.WriteToFile(savePath + "carvingIndex.jpg");
            //bumpIndex.WriteToFile(savePath + "bumpIndex.jpg");
            var bumpMap = BumpMapFilter(bumpIndex);
            textureAdded = textureAdded + bumpMap - 128;
            textureAdded.WriteToFile(savePath+"Texturetransfered.jpg");
            return textureAdded;
        }

        public static NetVips.Image BumpMapFilter(NetVips.Image img)
        {
            //var img = NetVips.Image.NewFromFile(filename);
            if (img.HasAlpha())
            {
                img = img.Flatten();
            }
            img = img.Colourspace("b-w");
            var kernel_small = Image.NewFromArray(new[,]
                                            {
                                                {0.0, 0.0, -1.0},
                                                {0.0, 0.0, 0.0},
                                                {1.0, 0.0, 0.0}
                                            }, scale: 0.25, offset: 1);

            var kernel_medium = Image.NewFromArray(new[,]
                                {
                                                {0, 0, 0, -1},
                                                {0, 0, 0, 0},
                                                {0, 0, 0, 0},
                                                {1, 0, 0, 0},
                                            }, scale: 0.5);

            var kernel_large = Image.NewFromArray(new[,]
                                            {
                                                {0, 0, 0, 0, -1},
                                                {0, 0, 0, 0, 0},
                                                {0, 0, 0, 0, 0},
                                                {0, 0, 0, 0, 0},
                                                {1, 0, 0, 0, 0}
                                            }, scale: 0.5);

            var bump = img.Conv(kernel_small, precision: "float") + 128;
            //conved = conved.Ifthenelse(conved, 128);
            //bump.WriteToFile(savePath+"BumpedFilter.png");
            return bump;
        }

        public static NetVips.Image CarvingMapFilter(NetVips.Image img)
        {
            if (img.HasAlpha())
            {
                img = img.Flatten();
            }
            img = img.Colourspace("b-w");

            var bump_kernel_small = Image.NewFromArray(new[,]
                                            {
                                                {0.0, 0.0, -1.0},
                                                {0.0, 0.0, 0.0},
                                                {1.0, 0.0, 0.0}
                                            }, scale: 0.25, offset: 0);

            var moveRight = img.Wrap(x: 1, y: 0);
            var moveUp = img.Wrap(x: 0, y: 1);
            var outline = (img - moveRight).Abs() + (img - moveUp).Abs();
            outline = outline.Ifthenelse(200, 255).Gaussblur(1);

            var carving = outline.Conv(bump_kernel_small, precision: "float") + 128;


            carving.WriteToFile(savePath+"CarvingFilter.jpg");
            return carving;

        }
    }
}
